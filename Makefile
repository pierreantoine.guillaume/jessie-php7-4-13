.PHONY: help start down shell stop ssh jessie-php7 clean
.SILENT: var/start

include .env
export $(shell sed 's/=.*//' .env)

all:jessie-php7

help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_\/-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

setup:  var/setup ## Setup test container

stop: clean ## Stop test container
	docker-compose stop

down: clean ## Destroy test container
	docker-compose down

clean: ## clean everything
	rm -rf var

shell: ## Open shell
	docker-compose exec jessie bash

jessie-php7:var/jessie-php7|var/known_hosts ## run jessie-php7

var/jessie-php7:ansible $(shell find ansible -name '*.yml') var/setup |var
	touch $@
	ansible-playbook -v -i ansible/hosts ansible/jessie-php7.yml

var/known_hosts:|var
	touch $@

var/setup: docker-compose.yml $(shell find docker) $(shell find ansible)
	make --no-print-directory down
	make --no-print-directory var
	touch $@
	docker-compose up -d
	docker-compose build

var:
	mkdir -p $@
