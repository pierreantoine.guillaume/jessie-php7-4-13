# JessiePHP7413

Ce repo aide au développement du playbook /ansible/jessie-php7.yml

Les données ansible sont dans le repertoire /ansible

## Usage (Compile Test Commit)

Pour lancer le playbook en mode test, il faut faire 

```shell
make
# ou
make jessie-php7
```

Il va lancer le container et lancer ansible dessus.

Si les dossiers ansible ou docker ont été touchés, il va détruire le container et le relancer.

## Docker

Le container docker ne contient rien d'autre que debian jessie, sshd et python-apt ; ce qui permet de tester tout le playbook ansible lors de l'installation.

Pour forcer la destruction du container docker à la prochaine session, il faut faire

```shell
touch docker
# ou
touch ansible
```

# Makefile

y'a quelques constructions sympa dans le makefile, par exemple

```makefile
include .env
export $(shell sed 's/=.*//' .env)
```

Qui source le `.env`, et en exporte toutes ses variables pour le reste des commandes.

